<?php
// show error reporting
error_reporting(E_ALL);
 
// set your default time-zone
date_default_timezone_set('Asia/Bangkok');
 
// variables used for jwt
$key = "example_key";
$iss = "api.wnp.test"; // this can be the servername
$aud = "Wonnapob";
$iat = time();  // issued at
$nbf = $iat; //not before in seconds
$exp = $iat + 3600; // expire time in seconds

// home page url
$home_url="http://api.wnp.test:8085/api/";

// page given in URL parameter, default page is one
$page = isset($_GET['page']) ? $_GET['page'] : 1;

// set number of records per page
$records_per_page = 5;

// calculate for the query LIMIT clause
$from_record_num = ($records_per_page * $page) - $records_per_page;
?>