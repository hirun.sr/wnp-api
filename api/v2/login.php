<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

include_once 'config/database.php';
include_once 'objects/user.php';



$app->get('/v2/login', function (Request $request, Response $response) {
    include_once 'config/core.php';
    // get database connection
    $database = new Database();
    $db = $database->getConnection();

    // instantiate user object
    $user = new User($db);

    $email = isset($_GET['email'])?trim($_GET['email']):'';
    $password = isset($_GET['password'])?trim($_GET['password']):'';

    if (empty($email) || empty($password)) {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code"=>400,"message" => "Email and/or Password is empty")));
    }

    // set product property values
    $user->email = $email;
    $email_exists = $user->emailExists();
    if ($email_exists && password_verify($password, $user->password)) {
        $token = array(
            "iss" => $iss,
            "aud" => $aud,
            "iat" => $iat,
            "nbf" => $nbf,
            "exp" => $exp,
            "data" => array(
              "id" => $user->id,
              "firstname" => $user->firstname,
              "lastname" => $user->lastname,
              "email" => $user->email
            )
        );

        $jwt = JWT::encode($token, $key);

        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array(
          "code"=>200,
          "message" => "Successful login.",
          "token"=>$jwt,
        )));
    } else {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code"=>401,"message" => "Login failed")));
    }
});
