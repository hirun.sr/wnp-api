<?php
class Product
{
    private $conn;

    public $id;
    public $name;
    public $description;
    public $price;
    public $category_id;
    public $category_name;
    public $created;

    // constructor
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function read()
    {
        $sql = "
            SELECT c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM products p
            LEFT JOIN categories c ON p.category_id = c.id
            ORDER BY p.created DESC
        ";

        try {
            // prepare the query
            $stmt = $this->conn->prepare($sql);

            // execute the query
            $stmt->execute();

            // get number of rows
            $num = $stmt->rowCount();

            if ($num > 0) {

                // products array
                $products_arr = array();
                $products_arr["total"] = 0;
                $products_arr["records"] = array();
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);

                    $product_item = array(
                        "id" => $id,
                        "name" => $name,
                        "description" => html_entity_decode($description),
                        "price" => $price,
                        "category_id" => $category_id,
                        "category_name" => $category_name,
                    );

                    array_push($products_arr["records"], $product_item);
                }
                $products_arr['total'] = $num;

                return $products_arr;

            } else {
                return null;
            }

        } catch (Exception $e) {
            //echo $e->getMessage();
            return null;
        }
    }

    public function readOne()
    {
        if (is_null($this->id)) {
            return null;
        }
        $sql = "
            SELECT c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM products p
            LEFT JOIN categories c ON p.category_id = c.id
            WHERE p.id = ?
            LIMIT 0,1
        ";

        try {
            // prepare query statement
            $stmt = $this->conn->prepare($sql);

            // bind id of product to be updated
            $stmt->bindParam(1, $this->id);

            // execute query
            $stmt->execute();

            // get retrieved row
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // set values to object properties
            $this->name = $row['name'];
            $this->price = $row['price'];
            $this->description = $row['description'];
            $this->category_id = $row['category_id'];
            $this->category_name = $row['category_name'];
        } catch (Exception $e) {
            //echo $e->getMessage();
            return null;
        }
    }

    public function create()
    {
        // query to insert record
        $sql = "
            INSERT INTO products SET
            name=:name,
            price=:price,
            description=:description,
            category_id=:category_id,
            created=:created
        ";
        try {
            // prepare query
            $stmt = $this->conn->prepare($sql);
            // sanitize
            $this->name = htmlspecialchars(strip_tags($this->name));
            $this->price = htmlspecialchars(strip_tags($this->price));
            $this->description = htmlspecialchars(strip_tags($this->description));
            $this->category_id = htmlspecialchars(strip_tags($this->category_id));
            $this->created = htmlspecialchars(strip_tags($this->created));

            // bind values
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":description", $this->description);
            $stmt->bindParam(":category_id", $this->category_id);
            $stmt->bindParam(":created", $this->created);

            // execute query
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            //echo $e->getMessage();
            return false;
        }

    }

    public function update()
    {
        // query to insert record
        $sql = "
            UPDATE products SET
            name=:name,
            price=:price,
            description=:description,
            category_id=:category_id
            WHERE id=:id
        ";
        try {
            // prepare query
            $stmt = $this->conn->prepare($sql);
            // sanitize
            $this->name = htmlspecialchars(strip_tags($this->name));
            $this->price = htmlspecialchars(strip_tags($this->price));
            $this->description = htmlspecialchars(strip_tags($this->description));
            $this->category_id = htmlspecialchars(strip_tags($this->category_id));
            $this->id=htmlspecialchars(strip_tags($this->id));

            // bind values
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":price", $this->price);
            $stmt->bindParam(":description", $this->description);
            $stmt->bindParam(":category_id", $this->category_id);
            $stmt->bindParam(':id', $this->id);

            // execute query
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            //echo $e->getMessage();
            return false;
        }

    }

    public function delete()
    {
        // query to insert record
        $sql = "
            DELETE FROM products WHERE id=:id
        ";
        try {
            // prepare query
            $stmt = $this->conn->prepare($sql);
            // sanitize
            $this->id=htmlspecialchars(strip_tags($this->id));

            // bind values
            $stmt->bindParam(':id', $this->id);

            // execute query
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            //echo $e->getMessage();
            return false;
        }

    }

    // read products with pagination
    public function readPaging($from_record_num, $records_per_page){
    
        // select query
        $sql = "
            SELECT c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
            FROM products p
            LEFT JOIN categories c ON p.category_id = c.id
            ORDER BY p.created DESC
            LIMIT ?, ?
        ";

        try {
            // prepare query statement
            $stmt = $this->conn->prepare( $sql );
            // bind variable values
            $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
            $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

            // execute query
            $stmt->execute();

            $num = $stmt->rowCount();
            // check if more than 0 record found
            if ($num>0) {
                // products array
                $records=array();

                // retrieve our table contents
                // fetch() is faster than fetchAll()
                // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                    // extract row
                    // this will make $row['name'] to
                    // just $name only
                    extract($row);   
                    $product_item=array(
                        "id" => $id,
                        "name" => $name,
                        "description" => html_entity_decode($description),
                        "price" => $price,
                        "category_id" => $category_id,
                        "category_name" => $category_name
                    );
             
                    array_push($records, $product_item);                                    
                }
                return $records;
                
            } else {
                return null;    
            }
    
        } catch (Exception $e) {
            //echo $e->getMessage();
            return null;           
        }
    

    

    

    }    

    // used for paging products
    public function count(){
        $sql = "SELECT COUNT(*) as total_rows FROM products";
    
        $stmt = $this->conn->prepare( $sql );
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        return $row['total_rows'];
    }
}
