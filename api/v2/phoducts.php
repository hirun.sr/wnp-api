<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;

include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'shared/utilities.php';

// get all products
$app->get('/v2/products', function (Request $request, Response $response) {
    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);
    $product_arr = $product->read();

    if (!is_null($product_arr)) {
        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array(
          "code" => 200,
          "message" => "OK",
          "result"=>$product_arr,
        )));
    } else {
        return $response->withStatus(404)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 404,"message" => "Data not found.")));
    }
});

// get product by id
$app->get('/v2/products/{id}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];

    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);
    $product->id = $id;
    $product->readOne();

    if (!$product->name) {
        return $response->withStatus(404)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 404,"message" => "Data not found.")));
    }

    // create array
    $product_arr = array(
        "id" => $product->id,
        "name" => $product->name,
        "description" => $product->description,
        "price" => $product->price,
        "category_id" => $product->category_id,
        "category_name" => $product->category_name,
    );
    return $response->withStatus(200)
    ->withHeader('Content-Type', 'application/json')
    ->write(json_encode(array(
      "code" => 200,
      "message" => "OK",
      "result"=>$product_arr,
    )));
});

// get product pagination
$app->get('/v2/products/page/{page}', function (Request $request, Response $response, array $args) {
    $page = $args['page'];

    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);

    $total_rows=$product->count();

    if ($total_rows > 0) {
        // products array
        $product_arr=array();
        $product_arr["total_rows"]=array();
        $product_arr["records"]=array();
        $product_arr["paging"]=array();

        $product_arr["records"] = $product->readPaging($from_record_num, $records_per_page);
        // include paging
        $page_url="{$home_url}v1/products.php?";
        $product_arr["paging"] = $utilities->getPaging($page, $total_rows, $records_per_page, $page_url);
        $product_arr["total_rows"] = $total_rows;

        return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array(
          "code" => 200,
          "message" => "OK",
          "result"=>$product_arr,
        )));
    } else {
        return $response->withStatus(404)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 404,"message" => "Data not found.")));
    }
});

// add product
$app->post('/v2/products', function (Request $request, Response $response) {
    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);


    // get posted data
    $data = json_decode(file_get_contents("php://input"));


    if (
        !empty($data->name) &&
        !empty($data->price) &&
        !empty($data->description) &&
        !empty($data->category_id)
    ) {
        // set product property values
        $product->name = $data->name;
        $product->price = $data->price;
        $product->description = $data->description;
        $product->category_id = $data->category_id;
        $product->created = date('Y-m-d H:i:s');
        if ($product->create()) {
            return $response->withStatus(201)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 201,"message" => "Product was created.")));
        } else {
            return $response->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 500,"message" => "Unable to create product.")));
        }
    } else {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 400,"message" => "Unable to create product. Data is incomplete.")));
    }
});

// update product
$app->put('/v2/products', function (Request $request, Response $response) {
    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);


    // get posted data
    $data = json_decode(file_get_contents("php://input"));

    if (
        !empty($data->id) &&
        !empty($data->name) &&
        !empty($data->price) &&
        !empty($data->description) &&
        !empty($data->category_id)
    ) {
        // set ID property of product to be edited
        $product->id = $data->id;

        // set product property values
        $product->name = $data->name;
        $product->price = $data->price;
        $product->description = $data->description;
        $product->category_id = $data->category_id;
        if ($product->update()) {
            return $response->withStatus(201)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 201,"message" => "Product was updated.")));
        } else {
            return $response->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 500,"message" => "Unable to update product.")));
        }
    } else {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 400,"message" => "Unable to update product. Data is incomplete.")));
    }
});

// delete product
$app->delete('/v2/products', function (Request $request, Response $response) {
    include_once 'config/core.php';

    // utilities
    $utilities = new Utilities();

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    //authorize token
    $jwt = '';

    $authHeader = $request->getHeader('authorization');
    if ($authHeader) {
        list($jwt) = sscanf($authHeader[0], 'Bearer %s');
    }
  
    // get token
    $token = isset($jwt) ? $jwt : "";


    if (!$token) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Authentication is required.")));
    }

    try {
        $decoded = JWT::decode($token, $key, array('HS256'));
    } catch (\Exception $e) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => $e->getMessage())));
    }

    $exp = $decoded->exp;
    //timeout
    if ($exp <= time()) {
        return $response->withStatus(401)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 401,"message" => "Token is timeout.")));
    }

    // instantiate product object
    $product = new Product($db);


    // get posted data
    $data = json_decode(file_get_contents("php://input"));
    if (!empty($data->id)) {
        // set ID property of product to be edited
        $product->id = $data->id;
        if ($product->delete()) {
            return $response->withStatus(200)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 200,"message" => "Product was deleted.")));
        } else {
            return $response->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(array("code" => 500,"message" => "Unable to delete product.")));
        }
    } else {
        return $response->withStatus(400)
        ->withHeader('Content-Type', 'application/json')
        ->write(json_encode(array("code" => 400,"message" => "Unable to delete product. Data is incomplete.")));
    }
});
