<?php
include_once 'config/database.php';
include_once 'objects/product.php';

include_once 'config/core.php';
include_once 'shared/utilities.php';
require "../../vendor/autoload.php";
use \Firebase\JWT\JWT;

//required headers
header("Access-Control-Allow-Origin: http://" . $_SERVER['HTTP_HOST'] . "/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// utilities
$utilities = new Utilities();

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$product = new Product($db);

$token = getBearerToken();
if (!$token) {
    // set response code
    http_response_code(401);
    echo json_encode(
        array(
            "code" => "401",
            "message" => "Authentication is required.",
        )
    );
    exit;
}
$decoded = null;
try {
    $decoded = JWT::decode($token, $key, array('HS256'));
} catch (\Exception $e) {
    http_response_code(401);
    echo json_encode(
        array(
            "code" => "401",
            "message" => "Authentication is required.",
            "exception" => $e->getMessage(),
        )
    );
    exit;
}

$exp = $decoded->exp;
//time out
if ($exp <= time()) {
    http_response_code(401);
    echo json_encode(
        array(
            "code" => "401",
            "message" => "Token is timeout",
        )
    );
    exit;
}

// get request method
$request_method = $_SERVER['REQUEST_METHOD'];

switch ($request_method) { // get products
    case 'GET':{
            if (isset($_GET['id'])) {  // get product by id
                 
                $product->id = $_GET['id'];
                $product->readOne();

                //print_r($product);
                if (!$product->name) {
                    http_response_code(404);
                    echo json_encode(
                        array(
                            "code" => "404",
                            "message" => "Data not found",
                        )
                    );
                    exit;
                }

                // create array
                $product_arr = array(
                    "id" => $product->id,
                    "name" => $product->name,
                    "description" => $product->description,
                    "price" => $product->price,
                    "category_id" => $product->category_id,
                    "category_name" => $product->category_name,

                );

                http_response_code(200);
                echo json_encode(
                    array(
                        "code" => "200",
                        "message" => "OK",
                        "result" => $product_arr,
                    )
                );
                exit;
            } elseif (isset($_GET['page'])) { // get paging products
                $total_rows=$product->count();

                if ($total_rows > 0) {
                    // products array
                    $product_arr=array();
                    $product_arr["total_rows"]=array();
                    $product_arr["records"]=array();
                    $product_arr["paging"]=array();

                    $product_arr["records"] = $product->readPaging($from_record_num, $records_per_page);
                    // include paging
                    $page_url="{$home_url}v1/products.php?";
                    $product_arr["paging"] = $utilities->getPaging($page, $total_rows, $records_per_page, $page_url);
                    $product_arr["total_rows"] = $total_rows;

                    http_response_code(200);
                    echo json_encode(
                        array(
                            "code" => "200",
                            "message" => "OK",
                            "result" => $product_arr,
                        )
                    );
                    exit;
                } else {
                    http_response_code(404);
                    echo json_encode(
                        array(
                            "code" => "404",
                            "message" => "Data not found",
                        )
                    );
                    exit;
                }
            } else { // get all products
             
                $product_arr = $product->read();

                if (is_null($product_arr)) {
                    http_response_code(404);
                    echo json_encode(
                        array(
                            "code" => "404",
                            "message" => "Data not found",
                        )
                    );
                    exit;
                } else {
                    http_response_code(200);
                    echo json_encode(
                        array(
                            "code" => "200",
                            "message" => "OK",
                            "result" => $product_arr,
                        )
                    );
                    exit;
                }
            }
            break;
        }
    case 'POST':{ //create new product

            // get posted data
            $data = json_decode(file_get_contents("php://input"));

            // make sure data is not empty
            if (
                !empty($data->name) &&
                !empty($data->price) &&
                !empty($data->description) &&
                !empty($data->category_id)
            ) {

                // set product property values
                $product->name = $data->name;
                $product->price = $data->price;
                $product->description = $data->description;
                $product->category_id = $data->category_id;
                $product->created = date('Y-m-d H:i:s');

                if ($product->create()) {
                    // set response code - 404 Not found
                    http_response_code(201);
                    echo json_encode(
                        array(
                            "code" => "201",
                            "message" => "Product was created.",
                        )
                    );
                    exit;
                } else {
                    // set response code - 404 Not found
                    http_response_code(500);
                    echo json_encode(
                        array(
                            "code" => "500",
                            "message" => "Unable to create product.",
                        )
                    );
                    exit;
                }
            } else {
                // set response code - 404 Not found
                http_response_code(400);
                echo json_encode(
                    array(
                        "code" => "400",
                        "message" => "Unable to create product. Data is incomplete.",
                    )
                );
                exit;
            }
            break;
        }
    case 'PUT':{ //update product

        // get id of product to be edited
        $data = json_decode(file_get_contents("php://input"));

        if (
            !empty($data->id) &&
            !empty($data->name) &&
            !empty($data->price) &&
            !empty($data->description) &&
            !empty($data->category_id)
        ) {
            // set ID property of product to be edited
            $product->id = $data->id;

            // set product property values
            $product->name = $data->name;
            $product->price = $data->price;
            $product->description = $data->description;
            $product->category_id = $data->category_id;

            if ($product->update()) {
                // set response code - 404 Not found
                http_response_code(201);
                echo json_encode(
                    array(
                        "code" => "201",
                        "message" => "Product was updated.",
                    )
                );
                exit;
            } else {
                // set response code - 404 Not found
                http_response_code(500);
                echo json_encode(
                    array(
                        "code" => "500",
                        "message" => "Unable to update product.",
                    )
                );
                exit;
            }
        } else {
            // set response code - 404 Not found
            http_response_code(400);
            echo json_encode(
                array(
                    "code" => "400",
                    "message" => "Unable to update product. Data is incomplete.",
                )
            );
            exit;
        }
        break;
    }
    case 'DELETE':{ //delete product

             // get id of product to be edited
             $data = json_decode(file_get_contents("php://input"));
             if (
                !empty($data->id)
            ) {
                 // set ID property of product to be edited
                 $product->id = $data->id;
                 if ($product->delete()) {
                     // set response code - 404 Not found
                     http_response_code(200);
                     echo json_encode(
                         array(
                            "code" => "200",
                            "message" => "Product was deleted.",
                        )
                    );
                     exit;
                 } else {
                     // set response code - 404 Not found
                     http_response_code(500);
                     echo json_encode(
                         array(
                            "code" => "500",
                            "message" => "Unable to deleted product.",
                        )
                    );
                     exit;
                 }
             } else {
                 // set response code - 404 Not found
                 http_response_code(400);
                 echo json_encode(
                     array(
                        "code" => "400",
                        "message" => "Unable to delete product. Data is incomplete.",
                    )
                );
                 exit;
             }
            break;
        }
    default:{ // method not allowed
        http_response_code(405);
        echo json_encode(
            array(
                "code" => "405",
                "message" => "Method not allowed",
            )
        );
        exit;
        break;
    }
}

/**
 * Get header Authorization
 * */
function getAuthorizationHeader()
{
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        //print_r($requestHeaders);
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    return $headers;
}

/**
 * get access token from header
 * */
function getBearerToken()
{
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}
