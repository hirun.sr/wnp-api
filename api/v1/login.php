<?php
include_once 'config/database.php';
include_once 'objects/user.php';
include_once 'config/core.php';

require "../../vendor/autoload.php";
use \Firebase\JWT\JWT;

//required headers
header("Access-Control-Allow-Origin: http://".$_SERVER['HTTP_HOST']."/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get request method
$request_method = $_SERVER['REQUEST_METHOD'];

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate user object
$user = new User($db);

switch ($request_method) {
    case 'GET': {
        
        $email = isset($_GET['email'])?trim($_GET['email']):'';
        $password = isset($_GET['password'])?trim($_GET['password']):'';

        if (empty($email) || empty($password)) {
            echo json_encode(
                array(
                    "code" => "400",
                    "message" => "Email and/or Password is empty",
                )
            );
            exit;
        }

        // set product property values
        $user->email = $email;
        $email_exists = $user->emailExists();   


        if ($email_exists && password_verify($password, $user->password)) {

            $token = array(
                "iss" => $iss,
                "aud" => $aud,
                "iat" => $iat,
                "nbf" => $nbf,
                "exp" => $exp,
                "data" => array(
                  "id" => $user->id,
                  "firstname" => $user->firstname,
                  "lastname" => $user->lastname,
                  "email" => $user->email
                )
              );
              
            // set response code
            http_response_code(200);

            // generate jwt
            $jwt = JWT::encode($token, $key);
            echo json_encode(
                array(
                "code" => "200",
                "message" => "Successful login.",
                "token" => $jwt
                )
            );
            exit;
           
        } else {

            // set response code
            http_response_code(401);
            echo json_encode(
                array(
                    "code" => "401",
                    "message" => "Login failed",
                )
            );
            exit;
        }

        break;
    }
    default : {
        // set response code - 404 Not found
        http_response_code(405);
        echo json_encode(
            array(
                "code" => "405",
                "message" => "Method not allowed"
            )
        );
        exit;
        break;
    }
}
?>